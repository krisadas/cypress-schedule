# cypress-schedule
https://krisadas.medium.com/schedule-cypress-run-with-gitlab-pipeline-schedule-d7c5b90b1f5b

#Schedule Cypress Run with Gitlab Pipeline Schedule Example

npx cypress open
npx cypress open --config-file prod.config.js
npx cypress open --config-file uat.config.js
npx cypress open --config-file dev.config.js

# Manually Record Run on cloud 
# Prod
npx cypress run --record --browser chrome --key YOUR_PROD_KEY
# UAT
npx cypress run --record --browser chrome --key YOUR_UAT_KEY
# Dev
npx cypress run --record --browser chrome --key YOUR_DEV_KEY

# Test
env: Prod
projectId: YOUR_PROD_PROJECT_ID
env: UAT 
projectId: YOUR_UAT_PROJECT_ID
env: Dev 
projectId: YOUR_DEV_PROJECT_ID



