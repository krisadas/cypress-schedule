const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: "YOUR_PROD_PROJECT_ID",
  e2e: {
    experimentalStudio: true,
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl: "https://example.cypress.io/",
    viewportWidth: 1440,
    viewportHeight: 768,
  },
  env: {
    ADD_NEW_TODO: "Feed the Cat",
  },
});
